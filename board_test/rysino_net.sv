/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module rysino_net (
    input wire clk,
    input wire rst,

    input wire [1:0]j5_rxd,
    input wire j5_crs_dv,
    input wire [1:0]j6_rxd,
    input wire j6_crs_dv,

	output logic [1:0]j5_txd,
    output logic j5_tx_en,
	output logic [1:0]j6_txd,
    output logic j6_tx_en
);

    rmii_echo j5 (
        .clk(clk),
        .rst(rst),
        .rxd(j5_rxd),
        .crs_dv(j5_crs_dv),
        .txd(j5_txd),
        .tx_en(j5_tx_en));

    rmii_echo j6 (
        .clk(clk),
        .rst(rst),
        .rxd(j6_rxd),
        .crs_dv(j6_crs_dv),
        .txd(j6_txd),
        .tx_en(j6_tx_en));

endmodule

`default_nettype wire
