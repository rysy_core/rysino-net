/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module rmii_echo (
	input wire clk,
	input wire rst,

	input wire [1:0]rxd,
	input wire crs_dv,

	output logic [1:0]txd,
	output logic tx_en
);
    logic [1:0]rxd_r;
    logic [1:0]crs_reg;

    always_ff @(posedge clk or negedge rst)
        if (!rst)
            {crs_reg, tx_en} <= '0;
        else begin
            crs_reg <= {crs_reg[0], crs_dv};
            tx_en <= |crs_reg;
        end

    always_ff @(posedge clk)
        {txd, rxd_r} <= {rxd_r, rxd};

endmodule

`default_nettype wire
